﻿namespace JoelDroidLollipopBatchDeodexer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label_Rom_Path = new System.Windows.Forms.Label();
            this.button_Browse_RomPath = new System.Windows.Forms.Button();
            this.romPathBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.button_Start_DeOdex = new System.Windows.Forms.Button();
            this.backgroundDeOdexWorker = new System.ComponentModel.BackgroundWorker();
            this.logListBox = new System.Windows.Forms.ListBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label_Java_CurrentVersion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 257);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Rom System Path: ";
            // 
            // label_Rom_Path
            // 
            this.label_Rom_Path.AutoSize = true;
            this.label_Rom_Path.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Rom_Path.Location = new System.Drawing.Point(221, 257);
            this.label_Rom_Path.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Rom_Path.Name = "label_Rom_Path";
            this.label_Rom_Path.Size = new System.Drawing.Size(160, 24);
            this.label_Rom_Path.TabIndex = 3;
            this.label_Rom_Path.Text = "Select Rom Folder";
            // 
            // button_Browse_RomPath
            // 
            this.button_Browse_RomPath.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Browse_RomPath.Location = new System.Drawing.Point(848, 249);
            this.button_Browse_RomPath.Margin = new System.Windows.Forms.Padding(4);
            this.button_Browse_RomPath.Name = "button_Browse_RomPath";
            this.button_Browse_RomPath.Size = new System.Drawing.Size(141, 37);
            this.button_Browse_RomPath.TabIndex = 4;
            this.button_Browse_RomPath.Text = "Browse";
            this.button_Browse_RomPath.UseVisualStyleBackColor = true;
            this.button_Browse_RomPath.Click += new System.EventHandler(this.button_Browse_RomPath_Click);
            // 
            // button_Start_DeOdex
            // 
            this.button_Start_DeOdex.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Start_DeOdex.Location = new System.Drawing.Point(21, 299);
            this.button_Start_DeOdex.Margin = new System.Windows.Forms.Padding(4);
            this.button_Start_DeOdex.Name = "button_Start_DeOdex";
            this.button_Start_DeOdex.Size = new System.Drawing.Size(170, 37);
            this.button_Start_DeOdex.TabIndex = 5;
            this.button_Start_DeOdex.Text = "Start Process";
            this.button_Start_DeOdex.UseVisualStyleBackColor = true;
            this.button_Start_DeOdex.Click += new System.EventHandler(this.button_Start_DeOdex_Click);
            // 
            // backgroundDeOdexWorker
            // 
            this.backgroundDeOdexWorker.WorkerReportsProgress = true;
            this.backgroundDeOdexWorker.WorkerSupportsCancellation = true;
            this.backgroundDeOdexWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundDeOdexWorker_DoWork);
            this.backgroundDeOdexWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundDeOdexWorker_ProgressChanged);
            this.backgroundDeOdexWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundDeOdexWorker_RunWorkerCompleted);
            // 
            // logListBox
            // 
            this.logListBox.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logListBox.FormattingEnabled = true;
            this.logListBox.ItemHeight = 24;
            this.logListBox.Location = new System.Drawing.Point(225, 299);
            this.logListBox.Margin = new System.Windows.Forms.Padding(4);
            this.logListBox.Name = "logListBox";
            this.logListBox.Size = new System.Drawing.Size(763, 244);
            this.logListBox.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::JoelDroidLollipopBatchDeodexer.Properties.Resources.LBDBannerSM;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 1);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1000, 204);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 221);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "Java Version: ";
            // 
            // label_Java_CurrentVersion
            // 
            this.label_Java_CurrentVersion.AutoSize = true;
            this.label_Java_CurrentVersion.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Java_CurrentVersion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label_Java_CurrentVersion.Location = new System.Drawing.Point(221, 221);
            this.label_Java_CurrentVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Java_CurrentVersion.Name = "label_Java_CurrentVersion";
            this.label_Java_CurrentVersion.Size = new System.Drawing.Size(73, 24);
            this.label_Java_CurrentVersion.TabIndex = 9;
            this.label_Java_CurrentVersion.Text = "Version";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1005, 563);
            this.Controls.Add(this.label_Java_CurrentVersion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.logListBox);
            this.Controls.Add(this.button_Start_DeOdex);
            this.Controls.Add(this.button_Browse_RomPath);
            this.Controls.Add(this.label_Rom_Path);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "JoelDroid Lollipop Batch Deodexer V2.4";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_Rom_Path;
        private System.Windows.Forms.Button button_Browse_RomPath;
        private System.Windows.Forms.FolderBrowserDialog romPathBrowserDialog;
        private System.Windows.Forms.Button button_Start_DeOdex;
        private System.ComponentModel.BackgroundWorker backgroundDeOdexWorker;
        private System.Windows.Forms.ListBox logListBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_Java_CurrentVersion;
    }
}

