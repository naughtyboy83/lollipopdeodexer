﻿using JoelDroidLollipopBatchDeodexer.LBDObjects;
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace JoelDroidLollipopBatchDeodexer.LBDUtilities
{
    public class SevenZipUtility
    {
        public static bool UnzipOdex(string odexName)
        {
            bool unzipStatus = false;
            FileInfo fiSevenZip = new FileInfo(ToolPaths.SEVENZIP_PATH);
            if (fiSevenZip.Exists)
            {
                DirectoryInfo diOdex = new DirectoryInfo(string.Format(ToolPaths.UNZIP_ODEX_IN_FOLDER, odexName));
                FileInfo[] fiOdex = CommonUtility.GetFilesByExtensions(diOdex, false, "*.odex", "*.odex.xz");
                if (fiOdex.Length == 1)
                {
                    if (fiOdex[0].Extension == ".odex")
                    {
                        unzipStatus = true;
                    }
                    else
                    {
                        string commandString = "x \"" + fiOdex[0].FullName + "\"";

                        ProcessInput processIn = new ProcessInput();
                        processIn.FileName = fiSevenZip.FullName;
                        processIn.Arguments = commandString;
                        processIn.WorkingDirectory = fiOdex[0].Directory.FullName;

                        ProcessOutput processOut = CommonUtility.RunProcess(processIn);

                        string odexNameWithUnzippedExtension = fiOdex[0].FullName.Substring(0, (fiOdex[0].FullName.Length - fiOdex[0].Extension.Length));
                        FileInfo fiUnZippedOdex = new FileInfo(odexNameWithUnzippedExtension);
                        unzipStatus = fiUnZippedOdex.Exists;
                    }
                }
            }

            return unzipStatus;
        }

        public static bool CompressDeodexedJar(string appName)
        {
            
            FileInfo fiClassesDex = new FileInfo(string.Format(ToolPaths.CLASSES_DEX_PATH, appName));
            FileInfo fiJar = new FileInfo(string.Format(ToolPaths.WORKING_JAR_PATH, appName));
            return CompressOut(fiClassesDex, fiJar);
        }

        public static bool CompressDeodexedApk(string appName)
        {

            FileInfo fiClassesDex = new FileInfo(string.Format(ToolPaths.CLASSES_DEX_PATH, appName));
            FileInfo fiApk = new FileInfo(string.Format(ToolPaths.WORKING_APK_PATH, appName));
            return CompressOut(fiClassesDex, fiApk);
        }

        public static bool CompressDeodexedClasses2(string appName)
        {

            FileInfo fiClassesDex = new FileInfo(string.Format(ToolPaths.CLASSES2_DEX_PATH, appName));
            FileInfo fiJar = new FileInfo(string.Format(ToolPaths.WORKING_JAR_PATH, appName));
            return CompressOut(fiClassesDex, fiJar);
        }

        private static bool CompressOut(FileInfo fiClassesDex, FileInfo fiApp)
        {
            bool compressStatus = false;
            FileInfo fiSevenZip = new FileInfo(ToolPaths.SEVENZIP_PATH);
            string originalHash = getMD5Hash(fiApp);

            if (fiSevenZip.Exists && fiClassesDex.Exists && fiApp.Exists)
            {
                string commandString = "u -tzip \"" + fiApp.FullName + "\" \"" + fiClassesDex.FullName + "\"";

                ProcessInput processIn = new ProcessInput();
                processIn.FileName = fiSevenZip.FullName;
                processIn.Arguments = commandString;
                processIn.WorkingDirectory = fiApp.Directory.FullName;

                ProcessOutput processOut = CommonUtility.RunProcess(processIn);

                FileInfo fiCompressOut = new FileInfo(fiApp.FullName);

                string newHash = getMD5Hash(fiCompressOut);

                bool bothHashAreEqual = string.Equals(originalHash, newHash);
                compressStatus = (fiCompressOut.Exists && !bothHashAreEqual);
            }
            return compressStatus;
        }

        private static string getMD5Hash(FileInfo fiMD5)
        {
            string md5String = string.Empty;
            if (fiMD5.Exists)
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(fiMD5.FullName))
                    {
                        byte[] computedHash = md5.ComputeHash(stream);
                        md5String = BitConverter.ToString(computedHash).Replace("-", "").ToLower();
                    }
                }
            }
            return md5String;
        }
    }
}
