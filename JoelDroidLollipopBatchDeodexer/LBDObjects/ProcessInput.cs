﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JoelDroidLollipopBatchDeodexer.LBDObjects
{
    public class ProcessInput
    {
        public string FileName { get; set; }
        public string WorkingDirectory { get; set; }
        public string Arguments { get; set; }
    }
}
