﻿using System;

namespace JoelDroidLollipopBatchDeodexer
{
    public static class ToolPaths
    {
        public static string TOOLS_PATH = AppDomain.CurrentDomain.BaseDirectory + "tools";
        public static string WORKING_PATH = AppDomain.CurrentDomain.BaseDirectory + "working";

        public static string SEVENZIP_PATH = TOOLS_PATH + @"\7za.exe";
        public static string ZIPALIGN_PATH = TOOLS_PATH + @"\zipalign.exe";
        public static string OAT2DEXJAR_PATH = TOOLS_PATH + @"\oat2dex.jar";

        public static string BOOT_OAT_PATH = WORKING_PATH + @"\boot\boot.oat";
        public static string BOOT_OAT_ODEX_PATH = WORKING_PATH + @"\boot\odex";
        public static string BOOT_OAT_DEX_PATH = WORKING_PATH + @"\boot\dex";
        public static string UNZIP_ODEX_IN_FOLDER = WORKING_PATH + @"\{0}";
        public static string UNZIP_OUT_PATH = WORKING_PATH + @"\{0}\{0}.odex";

        public static string TEMP_DEX_PATH = WORKING_PATH + @"\{0}\{0}.dex";
        public static string CLASSES_DEX_PATH = WORKING_PATH + @"\{0}\classes.dex";
        public static string FRAMEWORK_CLASSES2_DEX_PATH = WORKING_PATH + @"\{0}\framework-classes2.dex";
        public static string CLASSES2_DEX_PATH = WORKING_PATH + @"\{0}\classes2.dex";
        public static string WORKING_APK_PATH = WORKING_PATH + @"\{0}\{0}.apk";
        public static string WORKING_JAR_PATH = WORKING_PATH + @"\{0}\{0}.jar";
    }
}
