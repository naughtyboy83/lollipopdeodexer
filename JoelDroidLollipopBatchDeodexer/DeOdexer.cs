﻿using JoelDroidLollipopBatchDeodexer.LBDUtilities;
using System;
using System.Collections.Generic;
using System.IO;

namespace JoelDroidLollipopBatchDeodexer
{
    public static class DeOdexer
    {
        #region System Apps

        public static int DeodexApp(DirectoryInfo appDirectory)
        {
            int errorCode = -1;
            bool copyStatus = FileOperations.CopyToWorkingDirectory(appDirectory);
            if (copyStatus)
            {
                bool unzipOdexStatus = SevenZipUtility.UnzipOdex(appDirectory.Name);
                if (unzipOdexStatus)
                {
                    bool dexConversionStatus = JarRunner.Oat2Dex(appDirectory.Name);
                    if (dexConversionStatus)
                    {
                        bool renameStatus = FileOperations.RenameFile(appDirectory.Name);
                        if (renameStatus)
                        {
                            bool compressedStatus = SevenZipUtility.CompressDeodexedApk(appDirectory.Name);
                            if (compressedStatus)
                            {
                                bool zipalignStatus = ZipAlignUtility.ZipAlignApk(appDirectory.Name);
                                bool moveStatus = FileOperations.MoveDeodexedApp(appDirectory);
                                if (zipalignStatus && moveStatus)
                                {
                                    errorCode = 0;
                                }
                                else
                                {
                                    errorCode = 5;
                                }
                            }
                            else
                            {
                                errorCode = 4;
                            }
                        }
                        else
                        {
                            errorCode = 6;
                        }
                    }
                    else
                    {
                        errorCode = 3;
                    }
                }
                else
                {
                    errorCode = 2;
                }
            }
            else
            {
                errorCode = 1;
            }
            return errorCode;
        }

        #endregion

        #region Framework Apps

        public static int DeodexFrameworkApp(FileInfo[] frameworkApp)
        {
            int errorCode = -1;
            string appName = frameworkApp[0].Name.Substring(0, (frameworkApp[0].Name.Length - 4));

            bool copyStatus = FileOperations.CopyFrameworkAppToWorkingDirectory(frameworkApp);
            if (copyStatus)
            {
                bool unzipStatus = SevenZipUtility.UnzipOdex(appName);
                if (unzipStatus)
                {
                    bool dexConversionStatus = JarRunner.Oat2Dex(appName);
                    if (dexConversionStatus)
                    {
                        bool renameStatus = FileOperations.RenameFile(appName);
                        if (renameStatus)
                        {
                            bool compressedStatus = SevenZipUtility.CompressDeodexedJar(appName);
                            if (compressedStatus)
                            {
                                bool zipalignStatus = ZipAlignUtility.ZipAlignApk(appName);
                                bool moveStatus = FileOperations.MoveDeodexedFrameworkApp(frameworkApp, appName);
                                if (zipalignStatus && moveStatus)
                                {
                                    errorCode = 0;
                                }
                                else
                                {
                                    errorCode = 5;
                                }
                            }
                            else
                            {
                                errorCode = 4;
                            }
                        }
                        else
                        {
                            errorCode = 6;
                        }
                    }
                    else
                    {
                        errorCode = 3;
                    }
                }
                else
                {
                    errorCode = 2;
                }
            }
            else
            {
                errorCode = 1;
            }
            return errorCode;
        }

        public static int DeodexBootFrameworkApp(FileInfo[] frameworkApp)
        {
            int errorCode = -1;
            string appName = frameworkApp[0].Name.Substring(0, (frameworkApp[0].Name.Length - 4));

            bool copyStatus = FileOperations.CopyFrameworkAppToWorkingDirectory(frameworkApp);
            if (copyStatus)
            {
                bool renameStatus = FileOperations.RenameFile(appName);
                if (renameStatus)
                {
                    bool compressedStatus = SevenZipUtility.CompressDeodexedJar(appName);
                    if (compressedStatus)
                    {
                        bool moveStatus = FileOperations.MoveDeodexedFrameworkApp(frameworkApp, appName);
                        if (moveStatus)
                        {
                            errorCode = 0;
                        }
                        else
                        {
                            errorCode = 5;
                        }
                    }
                    else
                    {
                        errorCode = 4;
                    }
                }
                else
                {
                    errorCode = 6;
                }
            }
            else
            {
                errorCode = 1;
            }
            return errorCode;
        }

        public static int DeodexClasses2(FileInfo[] frameworkApp)
        {
            int errorCode = -1;
            string appName = frameworkApp[0].Name.Substring(0, (frameworkApp[0].Name.Length - 4));

            bool copyStatus = FileOperations.CopyFrameworkAppToWorkingDirectory(frameworkApp);
            if (copyStatus)
            {
                bool renameStatus = FileOperations.RenameClasses2(appName);
                if (renameStatus)
                {
                    bool compressedStatus = SevenZipUtility.CompressDeodexedClasses2(appName);
                    if (compressedStatus)
                    {
                        bool moveStatus = FileOperations.MoveDeodexedFrameworkApp(frameworkApp, appName);
                        if (moveStatus)
                        {
                            errorCode = 0;
                        }
                        else
                        {
                            errorCode = 5;
                        }
                    }
                    else
                    {
                        errorCode = 4;
                    }
                }
                else
                {
                    errorCode = 6;
                }
            }
            else
            {
                errorCode = 1;
            }
            return errorCode;
        }
        
        #endregion

        #region Utility Methods

        public static string GetDeodexMessage(int errorCode)
        {
            string deOdexMessage = string.Empty;
            switch (errorCode)
            {
                case 1:
                    deOdexMessage = "Copy Error";
                    break;
                case 2:
                    deOdexMessage = "Odex Unzip Failed";
                    break;
                case 3:
                    deOdexMessage = "Oat2Dex Conversion Failed";
                    break;
                case 4:
                    deOdexMessage = "Re-Compression Failed";
                    break;
                case 5:
                    deOdexMessage = "Move Failed";
                    break;
                case 6:
                    deOdexMessage = "Rename Failed";
                    break;
                default:
                    deOdexMessage = "Unknown Error";
                    break;
            }
            return deOdexMessage;
        }

        #endregion
    }
}
